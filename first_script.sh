#!/bin/bash

# # Basics
# num=4
# if test $num -gt 5
# then
#     echo "yes"
# else
#     echo "no"
# fi

# num=4
# if [ $num -le 5 ]
# then
#     echo True
# else
#     echo False
# fi

# # if /etc/passwd exist then echo whew
# if [ -e /etc/passwd ]
# then
#     echo "whew"
# else
#     echo "uh-oh"
# fi

# # or
# if [ -e /etc/passwd ]
# then
#     echo "whew"
# fi

# # Single line
# [ ! -e /etc/passwd ] || echo "whew"



